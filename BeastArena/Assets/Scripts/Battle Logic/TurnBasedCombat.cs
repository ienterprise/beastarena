﻿using Assets.Scripts.Units.Player;
using Assets.Scripts.Units.Player.Characters;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnBasedCombat : MonoBehaviour
{
    public int selGridInt = 0;
    private BattlePhase currentBattlePhase;
    private string[] classes;
    private IPlayerCharacter character;

    // Use this for initialization
    void Start()
    {
        this.currentBattlePhase = BattlePhase.START;
        classes = Enum.GetNames(typeof(CharacterClass));
    }

    // Update is called once per frame
    void Update()
    {
        switch (currentBattlePhase)
        {
            case BattlePhase.START:
                break;

            case BattlePhase.PLAYERTURN:
                break;

            case BattlePhase.ENEMYTURN:
                break;

            case BattlePhase.LOSE:
                break;

            case BattlePhase.WIN:
                break;
        }
    }

    private void OnGUI()
    {
        int selection = GUI.SelectionGrid(new Rect(25, 25, 180, 90), selGridInt, classes, 2);

        if (selection != selGridInt)
        {
            selGridInt = selection;

            switch (selGridInt)
            {
                case 0:
                    character = new Brute();
                    break;

                case 1:
                    character = new Elf();
                    break;

                case 2:
                    character = new BeastMaster();
                    break;

                case 3:
                    character = new Sorcerer();
                    break;
            }
            Debug.Log(character.ToString());
        }
        GUILayout.Window(0, new Rect(230, 25, 210, 200), DoMyWindow,
            character != null ? character.ToString() : "");

        if (GUILayout.Button("Next Stage"))
        {
            if (currentBattlePhase == BattlePhase.START)
            {
                Debug.Log(character.ToString());
                currentBattlePhase = BattlePhase.PLAYERTURN;
                Debug.Log(character.ToString());
            }
        }
    }

    void DoMyWindow(int windowID)
    {
        if (character != null)
        {
            GUILayout.BeginVertical();

            GUILayout.FlexibleSpace();

            if (GUILayout.Button("Level up!"))
            {
                character.LevelUp();
            }

            GUILayout.EndVertical();        
        }
    }
}
