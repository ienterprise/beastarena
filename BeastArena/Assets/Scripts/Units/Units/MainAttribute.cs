﻿namespace Assets.Scripts.Units.Units
{
    public enum MainAttribute
    {
        Strength,
        Agility,
        Intelligence
    }
}
