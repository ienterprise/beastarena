﻿namespace Assets.Scripts.Units.Player
{
    using Assets.Scripts.Units.Units;

    public interface IPlayerCharacter : IBaseCharacter
    {
        CharacterClass CharacterClass { get; }
    }
}
