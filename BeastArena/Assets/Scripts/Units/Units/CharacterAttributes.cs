﻿namespace Assets.Scripts.Units.Units
{
    public enum CharacterAttributes
    {
        Strength,
        Agility,
        Intelligence
    }
}
