﻿namespace Assets.Scripts.Units.Player.Characters
{
    using System;
    using Assets.Scripts.Units.Units;

    public class BeastMaster : BaseCharacter, IPlayerCharacter
    {
        public BeastMaster()
        {
            this._strength = AllCharacterDetails.BeastMasterDefaultStrength;
            this._agility = AllCharacterDetails.BeastMasterDefaultAgility;
            this._intelligence = AllCharacterDetails.BeastMasterDefaultIntelligence;
            this._mainAttribute = MainAttribute.Strength;

            this._healthMultipler = AllCharacterDetails.BeastMasterHealthMultipler;
            this._defenseMultipler = AllCharacterDetails.BeastMasterDefenseMultipler;
            this._attackDamageMultipler = AllCharacterDetails.BeastMasterAttackDamageMultipler;

            LevelUp();
        }

        public CharacterClass CharacterClass
        {
            get
            {
                return CharacterClass.BeastMaster;
            }
        }
    }
}
