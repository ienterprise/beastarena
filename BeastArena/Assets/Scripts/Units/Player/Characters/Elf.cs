﻿namespace Assets.Scripts.Units.Player.Characters
{
    using Assets.Scripts.Units.Units;
    using System;

    public class Elf : BaseCharacter, IPlayerCharacter
    {
        public Elf()
        {
            this._strength = AllCharacterDetails.ElfDefaultStrength;
            this._agility = AllCharacterDetails.ElfDefaultAgility;
            this._intelligence = AllCharacterDetails.ElfDefaultIntelligence;
            this._mainAttribute = MainAttribute.Agility;

            this._healthMultipler = AllCharacterDetails.ElfHealthMultipler;
            this._defenseMultipler = AllCharacterDetails.ElfDefenseMultipler;
            this._attackDamageMultipler = AllCharacterDetails.ElfAttackDamageMultipler;

            LevelUp();
        }

        public CharacterClass CharacterClass
        {
            get
            {
                return CharacterClass.Elf;
            }
        }
    }
}
