﻿namespace Assets.Scripts.Units.Player
{
    public enum CharacterClass
    {
        Brute,
        Elf,
        BeastMaster,
        Sorcerer
    }
}
