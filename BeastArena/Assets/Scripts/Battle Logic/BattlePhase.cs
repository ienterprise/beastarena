﻿public enum BattlePhase
{
    START,
    PLAYERTURN,
    ENEMYTURN,
    LOSE,
    WIN
}
