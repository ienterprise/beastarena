﻿namespace Assets.Scripts.Units.Player.Characters
{
    using Assets.Scripts.Units.Units;
    using System;

    public class Brute : BaseCharacter, IPlayerCharacter
    {
        public Brute()
        {
            this._strength = AllCharacterDetails.BruteDefaultStrength;
            this._agility = AllCharacterDetails.BruteDefaultAgility;
            this._intelligence = AllCharacterDetails.BruteDefaultIntelligence;
            this._mainAttribute = MainAttribute.Strength;

            this._healthMultipler = AllCharacterDetails.BruteHealthMultipler;
            this._defenseMultipler = AllCharacterDetails.BruteDefenseMultipler;
            this._attackDamageMultipler = AllCharacterDetails.BruteAttackDamageMultipler;

            LevelUp();
        }

        public CharacterClass CharacterClass
        {
            get
            {
                return CharacterClass.Brute;
            }
        }
    }
}
