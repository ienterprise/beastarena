﻿namespace Assets.Scripts.Units.Units
{
    public interface IBaseCharacter
    {
        int Level { get; }

        string Name { get; }

        double Health { get; }

        int Defense { get; }

        double AttackDamage { get;}

        int Strength { get; }

        int Agility { get; }

        int Intelligence { get; }

        MainAttribute MainAttribute { get; }

        void LevelUp();

        void SetCharacterData(double multiplier, AdjustableCharacterData dataType);
    }
}
