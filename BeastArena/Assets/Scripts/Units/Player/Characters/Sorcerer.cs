﻿namespace Assets.Scripts.Units.Player.Characters
{
    using Assets.Scripts.Units.Units;
    using System;

    public class Sorcerer : BaseCharacter, IPlayerCharacter
    {
        public Sorcerer()
        {
            this._strength = AllCharacterDetails.SorcererDefaultStrength;
            this._agility = AllCharacterDetails.SorcererDefaultAgility;
            this._intelligence = AllCharacterDetails.SorcererDefaultIntelligence;
            this._mainAttribute = MainAttribute.Intelligence;

            this._healthMultipler = AllCharacterDetails.SorcererHealthMultipler;
            this._defenseMultipler = AllCharacterDetails.SorcererDefenseMultipler;
            this._attackDamageMultipler = AllCharacterDetails.SorcererAttackDamageMultipler;

            LevelUp();
        }

        public CharacterClass CharacterClass
        {
            get
            {
                return CharacterClass.Sorcerer;
            }
        }
    }
}
