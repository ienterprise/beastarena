﻿namespace Assets.Scripts.Units.Units
{
    using System;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;

    public abstract class BaseCharacter : IBaseCharacter
    {
        private int _level;

        protected string _name;
        protected double _health;
        protected int _defense;
        protected double _attackDamage;
        protected int _strength;
        protected int _agility;
        protected int _intelligence;
        protected MainAttribute _mainAttribute;

        protected double _healthMultipler;
        protected double _defenseMultipler;
        protected double _attackDamageMultipler;
        protected double _attackSpeedMutipler;

        public int Level
        {
            get
            {
                return this._level;
            }
        }

        public string Name
        {
            get
            {
                return this._name;
            }
        }

        public double Health
        {
            get
            {
                return this._health;
            }
        }

        public double AttackDamage
        {
            get
            {
                return this._attackDamage;
            }
        }

        public int Defense
        {
            get
            {
                return this._defense;
            }
        }

        public int Strength
        {
            get
            {
                return this._strength;
            }
        }
        
        public int Agility
        {
            get
            {
                return this._agility;
            }
        }

        public int Intelligence
        {
            get
            {
                return this._intelligence;
            }
        }
        
        public MainAttribute MainAttribute
        {
            get
            {
                return _mainAttribute;
            }
        }

        public void SetCharacterData(double multiplier, AdjustableCharacterData dataType)
        {
            switch (dataType)
            {
                case AdjustableCharacterData.Health:
                    this._health = multiplier * GetMainStats();
                    break;

                case AdjustableCharacterData.Defense:
                    this._defense = (int) (multiplier * this.Agility);
                    break;

                case AdjustableCharacterData.AttackDamage:
                    this._attackDamage = multiplier * GetMainStats();
                    break;
            }
        }
        
        public void LevelUp()
        {
            this._level++;
            LevelUpStats();

            foreach (AdjustableCharacterData type in Enum.GetValues(typeof(AdjustableCharacterData)))
            {
                SetCharacterData(type);
            }
        }

        private void LevelUpStats()
        {
            int levelUpConstant = AllCharacterDetails.StatsLevelUpAmount;

            switch (this._mainAttribute)
            {
                case MainAttribute.Strength:
                    this._strength += levelUpConstant * 2;
                    this._agility += levelUpConstant;
                    this._intelligence += levelUpConstant;
                    break;

                case MainAttribute.Agility:
                    this._strength += levelUpConstant;
                    this._agility += levelUpConstant * 2;
                    this._intelligence += levelUpConstant;
                    break;

                case MainAttribute.Intelligence:
                    this._strength += levelUpConstant;
                    this._agility += levelUpConstant;
                    this._intelligence += levelUpConstant * 2;
                    break;
            }
        }

        private void SetCharacterData(AdjustableCharacterData dataType)
        {
            switch (dataType)
            {
                case AdjustableCharacterData.Health:
                    this._health = this._healthMultipler * GetMainStats();
                    break;

                case AdjustableCharacterData.Defense:
                    this._defense = (int)(this._defenseMultipler * this.Agility);
                    break;

                case AdjustableCharacterData.AttackDamage:
                    this._attackDamage = this._attackDamageMultipler * GetMainStats();
                    break;
            }
        }

        private int GetMainStats()
        {
            switch (this._mainAttribute)
            {
                case MainAttribute.Strength:
                    return this.Strength;

                case MainAttribute.Agility:
                    return this.Agility;

                case MainAttribute.Intelligence:
                    return this.Intelligence;

                default:
                    return 0;
            }
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            PropertyInfo[] properties = this.GetType().GetProperties();
            var pattern = "([a-z?])[_ ]?([A-Z])";

            foreach (PropertyInfo property in properties)
            {
                string propName = Regex.Replace(property.Name, pattern, "$1 $2");

                string propInfo = propName + " : " + property.GetValue(this, null);
                stringBuilder.AppendLine(propInfo);
            }

            return stringBuilder.ToString();
        }
    }
}
