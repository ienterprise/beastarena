﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllCharacterDetails : MonoBehaviour {

    public static int StatsLevelUpAmount = 2;

    public static double BruteHealthMultipler = 2.5;
    public static double BruteDefenseMultipler = 2;
    public static double BruteAttackDamageMultipler = 1.6;
    public static int BruteDefaultStrength = 12;
    public static int BruteDefaultAgility = 7;
    public static int BruteDefaultIntelligence = 3;
           
    public static double BeastMasterHealthMultipler = 2.2;
    public static double BeastMasterDefenseMultipler = 1.8;
    public static double BeastMasterAttackDamageMultipler = 1.2;
    public static int BeastMasterDefaultStrength = 10;
    public static int BeastMasterDefaultAgility = 8;
    public static int BeastMasterDefaultIntelligence = 5;
           
    public static double ElfHealthMultipler = 1.9;
    public static double ElfDefenseMultipler = 1.2;
    public static double ElfAttackDamageMultipler = 2;
    public static int ElfDefaultStrength = 6;
    public static int ElfDefaultAgility = 12;
    public static int ElfDefaultIntelligence = 7;
          
    public static double SorcererHealthMultipler = 1.6;
    public static double SorcererDefenseMultipler = 1;
    public static double SorcererAttackDamageMultipler = 1.3;
    public static int SorcererDefaultStrength = 4;
    public static int SorcererDefaultAgility = 8;
    public static int SorcererDefaultIntelligence = 13;
}
