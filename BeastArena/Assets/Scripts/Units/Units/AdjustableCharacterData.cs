﻿namespace Assets.Scripts.Units.Units
{
    public enum AdjustableCharacterData
    {
        Health,
        Defense,
        AttackDamage,
    }
}
